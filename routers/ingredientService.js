const express = require('express');
const router = express.Router()
const apiAdapter = require('./apiAdapter')

const BASE_URL = 'http://component.killerbee:3000'
const api = apiAdapter(BASE_URL)

router.get('/ingredients', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.post('/ingredients/add', (req, res) => {
  api.post(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.put('/ingredients/:id', (req, res) => {
  api.put(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.delete('/ingredients/:id', (req, res) => {
  api.delete(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

module.exports = router