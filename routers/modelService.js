const express = require('express');
const router = express.Router()
const apiAdapter = require('./apiAdapter')

const BASE_URL = 'http://component.killerbee:3000'
const api = apiAdapter(BASE_URL)

router.get('/designs', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.post('/designs/add', (req, res) => {
  api.post(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.put('/designs/:id', (req, res) => {
  api.put(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.delete('/designs/:id', (req, res) => {
  api.delete(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

module.exports = router