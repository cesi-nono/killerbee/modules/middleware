const express = require('express');
const router = express.Router()
const apiAdapter = require('./apiAdapter')

const BASE_URL = 'http://component.killerbee:3000'
const api = apiAdapter(BASE_URL)

router.get('/processes', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.post('/processes/add', (req, res) => {
  api.post(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.put('/processes/:id', (req, res) => {
  api.put(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

router.delete('/processes/:id', (req, res) => {
  api.delete(req.path, req.body).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

module.exports = router