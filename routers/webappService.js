const express = require('express');
const router = express.Router()
const apiAdapter = require('./apiAdapter')

const BASE_URL = 'http://webapp.killerbee:3000'
const api = apiAdapter(BASE_URL)

router.get('*', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data)
  }).catch(err => res.send(err))
})

module.exports = router
