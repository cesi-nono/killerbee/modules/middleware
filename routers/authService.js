const express = require('express');
const apiAdapter = require('./apiAdapter')
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

const router = express.Router();
dotenv.config();

const BASE_URL = 'http://webapp.killerbee:3000'
const api = apiAdapter(BASE_URL)

router.get('/login', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data);
  }).catch(err => res.send(err))
})

router.get('/_nuxt/*', (req, res) => {
  api.get(req.path).then(resp => {
    res.send(resp.data);
  }).catch(err => res.send(err))
})

const BASE_URL2 = 'http://component.killerbee:3000'
const api2 = apiAdapter(BASE_URL2)

router.post('/users/create', (req, res) => {
  api2.post(req.path, req.body).then(resp => {
    res.send(resp.data);
  })
})

router.post('/login', (req, res) => {
  console.log("Res login: " + res.data);
  api2.post(req.path, req.body).then(resp => {
    console.log("Resp login: " + resp.data);
    if (resp.data.success) {
      const token = generateAccessToken({ username: req.body.email })
      
      res.json(token);
    } else {
      res.sendStatus(401).send(resp.data);
    }
  }).catch(err => res.send(err))
})

// Token check
// router.use(authenticateToken)

function generateAccessToken(username) {
  return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '900s' });
}

function authenticateToken(req, res, next) {
  console.log('Authenticate token')

  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    console.log(err)

    if (err) return res.sendStatus(403)

    req.user = user

    next()
  })
}

module.exports = router