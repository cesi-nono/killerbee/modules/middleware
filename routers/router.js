const express = require('express');
const router = express.Router();

const authService = require('./authService');
const processService = require('./processService');
const ingredientService = require('./ingredientService');
const modelService = require('./modelService');
const webappService = require('./webappService');

// Log middleware
router.use((req, res, next) => {
    console.log("Called: ", req.path);
    next();
})

// JWT auth middleware
router.use(authService);

// Gateway rerouting
router.use(processService);
router.use(ingredientService);
router.use(modelService);
router.use(webappService);

module.exports = router;