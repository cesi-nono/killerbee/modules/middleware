FROM node:11.13.0-alpine

# create destination directory
RUN mkdir -p /usr/src/node-app
WORKDIR /usr/src/node-app

# update and install dependency
RUN apk update && apk upgrade
RUN apk add git

# copy the app, note .dockerignore
COPY . /usr/src/node-app/
RUN npm install --verbose

EXPOSE 3000