const express = require('express');
const app = express();
const router = require('./routers/router')
const bodyParser = require('body-parser');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use(router)

console.log("API Gateway run on localhost:3000")

app.listen(3000);